const coin = {
    state: 0,
    flip: function() {
        // 1. One point: Randomly set your coin object's "state" property to be either 
        //    0 or 1: use "this.state" to access the "state" property on this object.
        this.state = Math.floor(Math.random() * 2)
        return this.state
    },
    toString: function() {
        // 2. One point: Return the string "Heads" or "Tails", depending on whether
        //    "this.state" is 0 or 1.
        if (this.flip() === 0) {
            return 'Heads'
        } else {
            return 'Tails'
        }
    },
    toHTML: function() {
        const image = document.createElement('img');
        // 3. One point: Set the properties of this image element to show either face-up
        //    or face-down, depending on whether this.state is 0 or 1.
        if (this.toString() === 'Heads') {
            image.classList.add('heads')
            image.src = 'images/heads.jpg'
            image.style.width = 50 + 'px'
            image.style.height = 50 + 'px'
        } else {
            image.classList.add('tails')
            image.src = 'images/tails.jpg'
            image.style.width = 50 + 'px'
            image.style.height = 50 + 'px'
        }
        return image
    }
};

function display20Flips() {
    let results = [];
    // 4. One point: Use a loop to flip the coin 20 times, each time displaying the result of the flip as a string on the page.  After your loop completes, return an array with the result of each flip.
    for (let i = 0; i < 20; i++) {
        document.write(coin.toString() + ', ')
        results.push(coin.toString())
    }
    console.log(results)
    return results
}
display20Flips()

let imageArea = document.getElementById('imgArea')

function display20Images() {
    const results = [];
    // 5. One point: Use a loop to flip the coin 20 times, and display the results of each flip as an image on the page.  After your loop completes, return an array with result of each flip.
    for (let i = 0; i < 20; i++) {
        imageArea.appendChild(coin.toHTML())
        results.push(coin.toHTML())
    }
    console.log(results)
    return results
}
display20Images()